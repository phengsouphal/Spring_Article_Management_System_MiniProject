package com.testarticlemanagement.article_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticleTestApplication.class, args);
	}
}
