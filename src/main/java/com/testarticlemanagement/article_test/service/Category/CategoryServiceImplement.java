package com.testarticlemanagement.article_test.service.Category;

import com.testarticlemanagement.article_test.model.Category;
import com.testarticlemanagement.article_test.repository.Category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService{

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category findOne(int id) {
        return categoryRepository.findOne(id);
    }

    @Override
    public List<Category> findAll() {

        return categoryRepository.findAll();
    }

    @Override
    public void add(Category category) {
        categoryRepository.add(category);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }
}
