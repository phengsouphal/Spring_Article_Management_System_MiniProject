package com.testarticlemanagement.article_test.service.Category;

import com.testarticlemanagement.article_test.model.Category;

import java.util.List;

public interface CategoryService {

    Category findOne(int id);
    List<Category> findAll();
    void add(Category category);
    void delete(int id);
}
