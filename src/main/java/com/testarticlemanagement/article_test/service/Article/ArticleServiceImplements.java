package com.testarticlemanagement.article_test.service.Article;

import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.model.ArticleFilter;
import com.testarticlemanagement.article_test.repository.Article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplements implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public void add(Article article) {
        articleRepository.add(article);
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }


}
