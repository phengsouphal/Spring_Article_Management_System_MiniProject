package com.testarticlemanagement.article_test.service.Article;

import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.model.ArticleFilter;

import java.util.List;

public interface ArticleService {
    void add(Article article);
    Article findOne(int id);
    List<Article> findAll();
    void delete(int id);
    void update(Article article);


}
