package com.testarticlemanagement.article_test.repository.Category;


import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepository {

    @Select("SELECT * FROM tbl_categories WHERE id=#{id}")
    @Results({
            @Result(property = "id",column = "id")
    })
    Category findOne(int id);


    @Select("SELECT * FROM tbl_categories")
    @Results({
            @Result(property = "id",column = "id")
    })
    List<Category> findAll();

    @Insert("INSERT INTO tbl_categories(name) VALUES (#{name});")
    void add(Category category);

    @Delete("DELETE FROM tbl_categories WHERE id=#{id}")
    void delete(int id);

}
