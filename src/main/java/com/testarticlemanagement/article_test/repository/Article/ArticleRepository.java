package com.testarticlemanagement.article_test.repository.Article;

import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.model.ArticleFilter;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface ArticleRepository {

    @Insert("INSERT INTO tbl_articles(title,category_id,description, author,created_date, thumbnail  ) VALUES( #{name},#{category.id},#{description},#{author},#{createdDate},#{thumnail})")
    void add(Article article);


    @Select("SELECT a.id, a.title, a.category_id, a.description, a.author, a.created_date, a.thumbnail, c.name category_name FROM tbl_articles a INNER JOIN tbl_categories c ON a.category_id=c.id WHERE a.id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "title"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "category.name", column = "category_name")

    })
    Article findOne(int id);


    @Select("SELECT a.id, a.title, a.category_id, a.description, a.author, a.created_date, a.thumbnail, c.name category_name FROM tbl_articles a INNER JOIN tbl_categories c ON a.category_id=c.id ORDER BY a.id ASC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "title"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "thumnail", column = "thumbnail"),
            @Result(property = "category.name", column = "category_name")

    })
    List<Article> findAll();


    @Delete("DELETE FROM tbl_articles WHERE id=#{id}")
    void delete(int id);

    @Update("update tbl_articles set title=#{name}, description=#{description}, author=#{author},thumbnail=#{thumnail}, category_id=#{category.id} where id=#{id}")
    void update(Article article);

}


