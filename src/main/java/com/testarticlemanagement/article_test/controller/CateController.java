package com.testarticlemanagement.article_test.controller;


import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.model.Category;
import com.testarticlemanagement.article_test.service.Category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.jws.WebParam;
import java.util.List;

@Controller
public class CateController {
    @Autowired
    private CategoryService categoryService;


    @GetMapping("/addcate")
    public String add(Model m){
        List<Category> categories=categoryService.findAll();
        m.addAttribute("categories",categories);
        m.addAttribute("category",new Category());
        return "addcate";
    }

    @PostMapping("/addcate")
    public String saveCate(@ModelAttribute Category category, Model m){


        categoryService.add(category);
        return "redirect:/addcate";
    }


    @GetMapping("/deletecate/{id}")
    public String delete(@PathVariable("id") int id){
        categoryService.delete(id);
        return "redirect:/addcate";
    }




}
