package com.testarticlemanagement.article_test.controller;

import com.testarticlemanagement.article_test.model.Article;
import com.testarticlemanagement.article_test.service.Article.ArticleService;
import com.testarticlemanagement.article_test.service.Category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {

    @Autowired
   private ArticleService articleService;

    @Autowired
    private CategoryService categoryService;

    @Value("${file.server.path}")
    private String serverPath;




    @GetMapping("/article")
    public String article(ModelMap m){
        List<Article> articles=articleService.findAll();
        System.out.println(articles);
        m.addAttribute("articles",articles);
        return "article";
    }
    @GetMapping("/article/{id}") //@RequestParam int id
    public String articleOne(@PathVariable("id") int id, Model model){
        model.addAttribute("article", articleService.findOne(id));
        return "viewArticle";
    }


    @GetMapping("/addarticle")
    public String add(Model m){
        m.addAttribute("formAdd",true);
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("article", new Article());
        return "addarticle";
    }


    @GetMapping(value = {"/","/home","/index"})
    public String redirectToHomePage(){

        return "redirect:/article";
    }

    @PostMapping("/addarticle")
    public String saveArticle(@RequestParam("image") MultipartFile thumnail, @Valid @ModelAttribute Article article, BindingResult result, Model m){

        if (result.hasErrors()){
            m.addAttribute("formAdd",true);
            m.addAttribute("categories",categoryService.findAll());
            m.addAttribute("article", article);
            return "addarticle";
        }
        if (thumnail.isEmpty()){
            return "addarticle";
        }
        else {

            String fileName = thumnail.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            fileName = UUID.randomUUID() + "." + extension;

            try{
                Files.copy(thumnail.getInputStream(), Paths.get(serverPath,fileName));
            }
            catch (IOException e){
                e.printStackTrace();
            }

            article.setThumnail("/image/"+fileName);
        }

        article.setCreatedDate(new Date().toString());
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        articleService.add(article);
        return "redirect:/article";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        articleService.delete(id);
        return "redirect:/article";
    }

    String existingPic="";
    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id, Model m){
        m.addAttribute("formAdd",false);
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("article",articleService.findOne(id));
        Article a = articleService.findOne(id);
        existingPic=a.getThumnail();
        return "addarticle";
    }

    @PostMapping("/update")
    public String saveChange(@RequestParam("image") MultipartFile thumail, @Valid @ModelAttribute Article article, BindingResult bindingResult, Model m){

        if(bindingResult.hasErrors()){
            m.addAttribute("formAdd", false);
            System.out.println(article.getCategory());
            m.addAttribute("categories",categoryService.findAll());
            m.addAttribute("article", article);


            return "addarticle";
        }
        else {

            String fileName = thumail.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            fileName = UUID.randomUUID() + "." + extension;
            if(!thumail.isEmpty()){
                try{
                    Files.copy(thumail.getInputStream(), Paths.get(serverPath,fileName));
                }
                catch (IOException e){
                    e.printStackTrace();
                }

                article.setThumnail("/image/"+fileName);
            }
            else
                article.setThumnail(existingPic);

        }



        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        //article.setCategory(articleService.findOne(article.getId()).getCreatedDate());
        articleService.update(article);
        return "redirect:/article";
        }


}
