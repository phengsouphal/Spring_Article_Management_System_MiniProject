package com.testarticlemanagement.article_test.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    //Kom Pong Som
    @Bean
    @Profile("kps")
    public DataSource dataSource(){
        DriverManagerDataSource db=new DriverManagerDataSource();
        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://127.0.0.1:5432/ams_kps");
        db.setUsername("postgres");
        db.setPassword("123");
        return db;
    }



    //In Memory DB
    @Bean
    @Profile("memory")
    public DataSource inMemory(){
        EmbeddedDatabaseBuilder builder=new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScripts("sql/table.sql","sql/Data.sql");
        return builder.build();
    }












}
