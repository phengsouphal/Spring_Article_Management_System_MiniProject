package com.testarticlemanagement.article_test.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {

    @NotNull
    private int id;
    @NotEmpty
    private String name;
    private Category category;
    @Size(min = 10,max = 80)
    private String description;

    @NotBlank
    private String author;
    private String createdDate;
    private String thumnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public Article(){

    }
    public Article(int id, String name,Category category, String description, String author, String createdDate,String thumnail) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
        this.thumnail = thumnail;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", thumnail='" + thumnail + '\'' +
                '}';
    }
}
